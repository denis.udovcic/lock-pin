const test1: (pin: string) => boolean = pin => {
  return Number(pin[0]) * Number(pin[1]) === 24;
};

const test2: (pin: string) => boolean = (pin: string) => {
  return Number(pin[3]) === Number(pin[1]) / 3;
};

const test3: (pin: string) => boolean = pin => {
  return Number(pin[0]) + Number(pin[2]) === Number(pin[4]) + Number(pin[3]);
};

const test4: (pin: string) => boolean = pin => {
  const sum = pin.split('').reduce((acc, val) => Number(acc) + Number(val), 0);
  return sum === 26;
};

const test5: (pin: string) => boolean = pin => {
  const pinList = pin.split('');

  return pinList.some(function(item) {
    return pinList.indexOf(item) !== pinList.lastIndexOf(item);
  });
};

export default [test1, test2, test3, test4, test5];
