"use strict";
exports.__esModule = true;
var tests_1 = require("./tests");
var myPin = [];
var testIt = function () {
    var _loop_1 = function (pin) {
        var pinString = pin.toString();
        if (tests_1["default"].every(function (test, i) {
            var result = test(pinString);
            return result;
        })) {
            myPin.push(pinString);
        }
    };
    for (var pin = 10000; pin < 100000; pin++) {
        _loop_1(pin);
    }
};
testIt();
console.log(myPin);
