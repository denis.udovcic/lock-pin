import tests from './tests';
const PIN_LENGTH = 5;

const myPin = [];

let start = 1;
let end = 10;

for (let i = PIN_LENGTH; i > 1; i--) {
    start *= 10;
    end *= 10;
}  
 
const testIt = () => {
  for (let pin = start; pin < end; pin++) {
    const pinString = pin.toString();
    if (tests.every(test => test(pinString))) {
      myPin.push(pinString);
    }
  }
};

testIt();
console.log(myPin);