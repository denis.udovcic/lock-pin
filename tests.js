"use strict";
exports.__esModule = true;
var test1 = function (pin) {
    return Number(pin[0]) * Number(pin[1]) === 24;
};
var test2 = function (pin) {
    return Number(pin[3]) === Number(pin[1]) / 3;
};
var test3 = function (pin) {
    return Number(pin[0]) + Number(pin[2]) === Number(pin[4]) + Number(pin[3]);
};
var test4 = function (pin) {
    var sum = pin.split('').reduce(function (acc, val) { return Number(acc) + Number(val); }, 0);
    return sum === 26;
};
var test5 = function (pin) {
    var pinList = pin.split('');
    return pinList.some(function (item, i) {
        return pinList.indexOf(item) === i;
    });
};
exports["default"] = [test1, test2, test3, test4, test5];
